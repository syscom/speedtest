// Copyright 2019 Silke Hofstra
//
// Licensed under the EUPL
//
// You may not use this work except in compliance with the licence.
// A copy of the license is included in the top level directory of this project.

// speedtest is a Go implementation of the HTTP legacy fallback for an Ookla speedtest server.
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"path"
	"strings"
)

var (
	staticDir string
	redirURL  string
)

// size calculates the size of an HTTP request
func size(r *http.Request) int {
	data, _ := ioutil.ReadAll(r.Body)
	return 501 + len(r.RequestURI) - len("/upload") + len(data)
}

// uploadHandler handles the data upload requests
func uploadHandler(w http.ResponseWriter, r *http.Request) {
	r.Body = http.MaxBytesReader(w, r.Body, 128<<20)
	fmt.Fprintf(w, "size=%d", size(r))
}

// staticHandler handles requests to static files
func staticHandler(w http.ResponseWriter, r *http.Request) {
	p := strings.TrimPrefix(r.URL.Path, "/")
	http.ServeFile(w, r, path.Join(staticDir, path.Clean(p)))
}

// indexHandler handles requests to the site index
func indexHandler(w http.ResponseWriter, r *http.Request) {
	if redirURL != "" && r.URL.Path == "/" {
		http.Redirect(w, r, redirURL, 302)
	} else {
		staticHandler(w, r)
	}
}

// listenAndServe listens and serves HTTP on the specified address
func listenAndServe(addr string) {
	log.Printf("Listening on %s", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}

func main() {
	var addresses string

	flag.StringVar(&addresses, "addrs", ":8080", "Listen addresses and ports separated by commas")
	flag.StringVar(&staticDir, "static", "static", "Path to directory containing static")
	flag.StringVar(&redirURL, "info-url", "", "URL to page with service information")
	flag.Parse()

	// HTTP endpoints
	http.HandleFunc("/upload", uploadHandler)
	http.HandleFunc("/", indexHandler)

	// Ookla-compatible endpoints
	http.HandleFunc("/upload.php", uploadHandler)
	http.HandleFunc("/upload.asp", uploadHandler)
	http.HandleFunc("/upload.aspx", uploadHandler)
	http.HandleFunc("/upload.jsp", uploadHandler)

	// Split list of addresses
	addrs := strings.Split(addresses, ",")

	// Listen on all configured addresses
	for _, addr := range addrs {
		go listenAndServe(addr)
	}

	select {}
}
