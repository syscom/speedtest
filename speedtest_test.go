package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type TestRequest struct {
	Method, URL      string
	BodySize, Result int
}

var testRequests = []*TestRequest{
	{"POST", "/upload", 515699, 516200},
	{"POST", "/upload", 1980899, 1981400},
}

func newRequest(t *TestRequest) *http.Request {
	body := bytes.NewBuffer(make([]byte, t.BodySize))
	return httptest.NewRequest(t.Method, t.URL, body)
}

func TestSize(t *testing.T) {
	for i, r := range testRequests {
		s := size(newRequest(r))
		if s != r.Result {
			t.Errorf("Size of test %v is %v, expected %v", i, s, r.Result)
		}
	}
}

func TestUploadHandler(t *testing.T) {
	for i, r := range testRequests {
		exp := fmt.Sprintf("size=%d", r.Result)
		rec := httptest.NewRecorder()
		req := newRequest(r)
		uploadHandler(rec, req)

		res, _ := ioutil.ReadAll(rec.Result().Body)
		if string(res) != exp {
			t.Errorf("Response of test %v is %q, expected %q", i, res, exp)
		}
	}
}

func testGet(p string, h func(http.ResponseWriter, *http.Request)) *http.Response {
	rec := httptest.NewRecorder()
	req := httptest.NewRequest("GET", p, nil)
	h(rec, req)
	return rec.Result()
}

func TestIndexHandler(t *testing.T) {
	redirURL = "http://test.invalid/info"

	res := testGet("/", indexHandler)
	loc, err := res.Location()
	if err != nil || res.StatusCode != 302 || loc.String() != redirURL {
		t.Errorf("Unexpected response: %#v", res)
	}
}

func TestStaticHandler(t *testing.T) {
	redirURL = ""
	staticDir = "."

	res1 := testGet("/speedtest.go", indexHandler)
	if res1.StatusCode != 200 {
		t.Errorf("Unexpected response: %#v", res1)
	}

	res2 := testGet("/speedtest.go", staticHandler)
	if res1.StatusCode != 200 {
		t.Errorf("Unexpected response: %#v", res2)
	}

	b1, _ := ioutil.ReadAll(res1.Body)
	b2, _ := ioutil.ReadAll(res2.Body)
	if string(b1) != string(b2) {
		t.Errorf("Handler mismatch: %q != %q", b1, b2)
	}
}
