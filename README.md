# speedtest
[![pipeline status](https://git.snt.utwente.nl/syscom/speedtest/badges/master/pipeline.svg)](https://git.snt.utwente.nl/syscom/speedtest/commits/master)
[![coverage report](https://git.snt.utwente.nl/syscom/speedtest/badges/master/coverage.svg)](https://git.snt.utwente.nl/syscom/speedtest/commits/master)

`speedtest` is a Go implementation of the HTTP legacy fallback for an Ookla speedtest server.
